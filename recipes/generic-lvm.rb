Chef::Application.fatal!('LVM enabled but not physical_volumes are defined') if node['gitlab_lvm']['configure'] && node['gitlab_lvm']['physical_volumes'].empty?

lvm_volume_group "#{node['gitlab_lvm']['name']}_vg" do
  only_if do node['gitlab_lvm']['configure'] end
  physical_volumes node['gitlab_lvm']['physical_volumes']
  wipe_signatures true

  logical_volume "#{node['gitlab_lvm']['name']}_data" do
    size        '100%FREE'
    filesystem  node['gitlab_lvm']['filesystem']
    mount_point location: node['gitlab_lvm']['mount_point'], options: node['gitlab_lvm']['mount_options']
    stripes     node['gitlab_lvm']['physical_volumes'].length
    stripe_size 256
  end
end

directory node['gitlab_lvm']['mount_point'] do
  only_if do node['gitlab_lvm']['configure'] end
  user  node['gitlab_lvm']['mount_user']
  group node['gitlab_lvm']['mount_group']
  mode  '0755'
end
