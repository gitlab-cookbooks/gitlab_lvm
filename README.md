# gitlab-lvm
Recipe to configure an lvm generically

## Attributes
By default the attributes are set so nothing happens. However, enabling the configure flag and setting the following will create an lvm:

```json
"gitlab_lvm":{
  "configure":true,
  "physical_volumes":["sdb","sdc"],
  "mount_point":"/var/opt/gitlab/",
  "mount_options":"noatime,nodiratime",
  "mount_user":"git",
  "mount_group":"git",
  "filesystem":"ext4",
  "name":"gitlab"
}
```

## Usage

### gitlab_lvm::generic-lvm

Include this in your run list to configure the above mentioned lvm.
