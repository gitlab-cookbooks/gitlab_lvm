default['gitlab_lvm']['configure'] = false
default['gitlab_lvm']['physical_volumes'] = []
default['gitlab_lvm']['mount_point'] = nil
default['gitlab_lvm']['mount_options'] = nil
default['gitlab_lvm']['mount_user'] = nil
default['gitlab_lvm']['mount_group'] = node['gitlab_lvm']['mount_user']
default['gitlab_lvm']['filesystem'] = nil
default['gitlab_lvm']['name'] = nil
